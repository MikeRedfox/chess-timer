# Chess Timer on a Site

The code is pretty straight-forward and I don't think it needs much explanation

You need to give two separate numbers as input 
+ The first being the duration of the game (in minutes)
+ The second being the increment on each move (in seconds)

It is thought to be used on a cellphone so it easier to click

Link to the site [here](https://mikeredfox.gitlab.io/chess-timer/)